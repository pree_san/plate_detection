import flask
from flask import request, jsonify
import mysql.connector
from mysql.connector import Error

app = flask.Flask(__name__)
app.config["DEBUG"] = True

#connect to db
try:
    connection = mysql.connector.connect(host='localhost', database='loginweb', user='root', password='')
    
    if connection.is_connected():
        db_info = connection.get_server_info()
        print("Connected to MYSQL Servier version ", db_info)
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)

except Error as e:
        print("Error while connecting to MySQL", e)


# Create some test data for catalog in the form of a list of dictionaries

books = [
    {
        'id' : 0,
        'title' : 'A Fire Upon the Deep',
        'author' : 'Vernor Vinge',
        'first_sentence' : 'The coldsleep itself was dreamless',
        'year_published' : '1992'   
    },
    {
        'id' : 1,
        'title' : 'The Ones who Walk Away From Omelas',
        'author' : 'Ursula K. Le Guin',
        'first_sentence' : 'WIth a clamor of bells that set',
        'year_published' : '1973' 
    },
    {
        'id' : 2,
        'title' : 'Dhalgren',
        'author' : 'Samuel R. Denaly',
        'first_sentence' : 'and bla bla bla',
        'year_published' : '1975' 
    }
]


@app.route('/', methods=['GET'])
def home():
    return "<h1>Distant Reading Archive</h1><p>This site is a prototype API for distant reading of science fiction novels.</p>"


@app.route('/api/v1/resources/books/all', methods=['GET'])
def api_all():
    return jsonify(books)

@app.route('/api/v1/resources/books', methods=['GET'])
# this route running like this: 127.0.0.1:5000/api/v1/resources/books?id=0 
def api_id():
    # Check if an ID was provided as part of the URL.
    # if ID is provided, assign it to a variable
    # if no ID is provided, display an error in the browser

    if 'id' in request. args:
        id = int(request.args['id'])
    else:
        return "Error: No id field provided. Please specify an id."
    
    # Create an empty list for our results
    results = []

    # Loop through the data and match results that fit the request ID
    # IDs are unique, but other fields might return many results
    for book in books:
        if book['id'] == id:
            results.append(book)
    
    # Use the jsonify function from Flask to convert our list of
    # Python dictionaries to the JSON format
    return jsonify(results)

@app.route('/api/v1/todatabase', methods=['GET'])
def connect_to_db(conn=connection):

    
    # finally:
    #     if connection.is_connected():
    #         cursor.close()
    #         connection.close()
    #         print("MySQL connection is closed")

    sqlQuery = "select * from mhsweb"
    cursor = conn.cursor()
    cursor.execute(sqlQuery)

    

    # get all records
    records = cursor.fetchall()
    print("Total number of rows in table: ", cursor.rowcount)

    results = []
    for row in records:
        ele_temp = {}
        ele_temp["nrp"] = row[0]
        ele_temp["usename"] = row[1]
        ele_temp["password"] = row[2]
        ele_temp["level"] = row[3]
        results.append(ele_temp)



    return jsonify(results)



app.run()